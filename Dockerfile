FROM nginx:alpine

# Install app dependencies
COPY index.html ./

# Expose port
EXPOSE 80

